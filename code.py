import board
import time
import usb_hid
import digitalio

from digitalio import DigitalInOut, Direction, Pull
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode

KEY_MUTE = 0x7F  # Only works on mac

# Connections left to right on KeyPad
# 1     2       3       4       5       6       7       8
# GP6   GP7     GP8     GP9     GP10    GP11    GP12    GP13 on RP pico


# Set up Rows
rows = []
for p in [board.GP13, board.GP12, board.GP11, board.GP10]:
    row = DigitalInOut(p)
    row.direction = Direction.OUTPUT
    rows.append(row)
# anodes OFF
for i in range(4):
    rows[i].value = 0

# Set up columns
cols = []
for p in [board.GP9, board.GP8, board.GP7, board.GP6]:
    col = DigitalInOut(p)
    col.direction = Direction.INPUT
    col.pull = Pull.DOWN
    cols.append(col)


keyboard = Keyboard(usb_hid.devices)

mute_led_pin = board.GP15  # pin to connect LED to


# Initializing LED
mute_led = digitalio.DigitalInOut(mute_led_pin)
mute_led.direction = digitalio.Direction.OUTPUT


def getkey():  # Returns -999 or key value
    values = [1, 2, 3, 10, 4, 5, 6, 11, 7, 8, 9, 12, 14, 0, 15, 13]
    val = -999  # Error value for no key press
    for count in range(10):  # Try to get key press 10 times
        for r in range(4):  # Rows, one at a time
            rows[r].value = 1  # row HIGH
            for c in range(4):  # Test columns, one at a time
                if cols[c].value == 1:  # Is column HIGH?
                    p = r * 4 + c  # Pointer to values list
                    val = values[p]
                    count = 11  # This stops looping
                    if count > 15:
                        print("")
                    mute_led.value = 1  # Flash LED ON if key pressed
            rows[r].value = 0  # row LOW
    time.sleep(0.2)  # Debounce
    mute_led.value = 0  # LED OFF
    return val


def getvalue(digits):  # Number of digits
    result = 0
    count = 0
    while True:
        x = getkey()
        if x != -999 and x < 10:  # Check if numeric key pressed
            result = result * 10 + x
            print(result)
            count = count + 1
        if count == digits:
            return result


def pressKey(*keyList):
    for key in keyList:
        keyboard.press(key)
    time.sleep(0.15)
    for key in keyList:
        keyboard.release(key)


characters = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "*",
    "#",
    "0",
]

while True:
    # Check if button is pressed and if it is, to press the Macros and toggle LED
    x = getkey()
    if x != -999:  # A key has been pressed!
        print(characters[x - 1])

    if x == 1:
        pressKey(KEY_MUTE)
    if x == 2:
        pressKey(Keycode.CONTROL, Keycode.ALT, Keycode.Q)

    time.sleep(0.1)
